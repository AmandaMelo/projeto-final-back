class SubjectSerializer < ActiveModel::Serializer
  attributes :id, :name, :depart_name, :workload

  has_many :prerequisites
  has_many :college_classes

  def depart_name
    User.find(self.object.user_id).name
  end
end
