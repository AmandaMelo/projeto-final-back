class StudentSerializer < ActiveModel::Serializer
  attributes :id, :course_id, :name_student, :name_course

  def name_course
    Course.find(self.object.course_id).name
  end

  def name_student
    User.find(self.object.user_id).name
  end

end
