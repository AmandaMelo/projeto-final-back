class CourseSerializer < ActiveModel::Serializer
  attributes :id, :name, :number_students, :coord_name, :code, :campus, :knowledge_area
  has_many :students
  has_many :course_disciplines

  def coord_name
    User.find(self.object.user_id).name
  end

end
