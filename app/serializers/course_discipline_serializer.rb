class CourseDisciplineSerializer < ActiveModel::Serializer
  attributes :id, :order
  belongs_to :subject
  has_one :course

end
