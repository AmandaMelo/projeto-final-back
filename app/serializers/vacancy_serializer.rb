class VacancySerializer < ActiveModel::Serializer
  attributes :id, :subject_name, :college_class_id, :class_name, :teacher_name, :course_id, :course_name, :number

  def class_name
    CollegeClass.find(self.object.college_class_id).name
  end

  def teacher_name
    Teacher.find(CollegeClass.find(self.object.college_class_id).teacher_id).name
  end

  def subject_name
    Subject.find(CollegeClass.find(self.object.college_class_id).subject_id).name
  end

  def course_name
    Course.find(self.object.course_id).name
  end

end
