class PrerequisitesSerializer < ActiveModel::Serializer
  attributes :requisite, :requisite_name

  def requisite_name
    Subject.find(self.object.requisite).name
  end
end
