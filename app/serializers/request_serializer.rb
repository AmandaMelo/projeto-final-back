class RequestSerializer < ActiveModel::Serializer
  attributes :id
  has_one :student
  has_one :college_class
end
