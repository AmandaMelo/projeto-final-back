class CollegeClassSerializer < ActiveModel::Serializer
  attributes :id
  belongs_to :teacher
  belongs_to :subject
end
