class VacanciesController < ApplicationController
  before_action :set_vacancy, only: [:show, :update, :destroy]
  # load_and_authorize_resource
  # GET /vacancies
  def index
    @vacancies = Vacancy.all

    render json: @vacancies
  end

  # GET /vacancies/1
  def show
    render json: @vacancy
  end

  # POST /vacancies
  def create
    @vacancy = Vacancy.new(vacancy_params)
    period_status = Period.find(CollegeClass.find(@vacancy.college_class_id).period_id)
    
    if period_status.planning?
      if @vacancy.save
        render json: @vacancy, status: :created, location: @vacancy
      else
        render json: @vacancy.errors, status: :unprocessable_entity
      end
    else
      render json: @vacancy.errors, status: :unprocessable_entity
    end
  end
  # PATCH/PUT /vacancies/1
  def update
    period_status = Period.find(CollegeClass.find(@vacancy.college_class_id).period_id)
    if period_status.planning?
      if @vacancy.update(vacancy_params)
        render json: @vacancy
      else
        render json: @vacancy.errors, status: :unprocessable_entity
      end
    else
      render json: @vacancy.errors, status: :unprocessable_entity
    end
  end
  # DELETE /vacancies/1
  def destroy
    period_status = Period.find(CollegeClass.find(@vacancy.college_class_id).period_id)
    if period_status.planning?
      @vacancy.destroy
    end
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vacancy
      @vacancy = Vacancy.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def vacancy_params
      params.require(:vacancy).permit(:college_class_id, :number, :course_id)
    end
end
