class CompletedSubjectsController < ApplicationController
  before_action :set_completed_subject, only: [:show, :update, :destroy]
  # load_and_authorize_resource
  # GET /completed_subjects
  def index
    @completed_subjects = CompletedSubject.all

    render json: @completed_subjects
  end

  # GET /completed_subjects/1
  def show
    render json: @completed_subject
  end

  # POST /completed_subjects
  def create
    @completed_subject = CompletedSubject.new(completed_subject_params)

    if @completed_subject.save
      render json: @completed_subject, status: :created, location: @completed_subject
    else
      render json: @completed_subject.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /completed_subjects/1
  def update
    if @completed_subject.update(completed_subject_params)
      render json: @completed_subject
    else
      render json: @completed_subject.errors, status: :unprocessable_entity
    end
  end

  # DELETE /completed_subjects/1
  def destroy
    @completed_subject.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_completed_subject
      @completed_subject = CompletedSubject.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def completed_subject_params
      params.require(:completed_subject).permit(:student_id, :subject_id)
    end
end
