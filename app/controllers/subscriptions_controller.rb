class SubscriptionsController < ApplicationController
  before_action :set_subscription, only: [:show, :update, :destroy]
  # load_and_authorize_resource
  # GET /subscriptions
  def index
    @subscriptions = Subscription.all

    render json: @subscriptions
  end

  # GET /subscriptions/1
  def show
    render json: @subscription
  end

  # POST /subscriptions
  def create
    @subscription = Subscription.new(subscription_params)
    @subscription.status = 0
    @subscription.period_id = CollegeClass.find(Vacancy.find(@subscription.vacancy_id).college_class_id).period_id

    period_status = Period.find(@subscription.period_id)
    if period_status.registration?
      if @subscription.save
        render json: @subscription, status: :created, location: @subscription
      else
        render json: @subscription.errors, status: :unprocessable_entity
      end
    else
      render json: @subscription.errors, status: :unprocessable_entity
    end
  end
  # PATCH/PUT /subscriptions/1
  def update
    period_status = Period.find(@subscription.period_id)
    if period_status.registration? || period_status.progress?
      period = Period.find(@subscription.period_id)
      unless period.concluded?
        if @subscription.update(subscription_params)
          render json: @subscription
        else
          render json: @subscription.errors, status: :unprocessable_entity
        end
      end
    else
      render json: @subscription.errors, status: :unprocessable_entity
    end
  end

  # DELETE /subscriptions/1
  def destroy
    period_status = Period.find(@subscription.period_id)
    if period_status.registration?
      @subscription.destroy
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subscription
      @subscription = Subscription.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def subscription_params
      params.require(:subscription).permit(:final_score, :grade_one, :grade_two, :status, :students_id, :vacancy_id, :teacher_id)
    end
end
