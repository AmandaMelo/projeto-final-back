class AuthenticationController < ApplicationController
    def login
        user = User.find_by(cpf: params[:user][:cpf])
        user = user&.authenticate(params[:user][:password])
        
        if user
          token = JsonWebToken.encode(user_id: user.id)
          render json: {token: token, user: {name: user.name, id: user.id, email: user.email, cpf: user.cpf, rg: user.rg, role: user.role, birthdate: user.birthdate, state: user.state, nationality: user.nationality}}
        else
          render json: {message: "CPF e/ou Senha incorretos!"}, status: 401
        end
    end
      
    private
    # Only allow a trusted parameter "white list" through.
        def user_paramsUser
            params.require(:user).permit(:cpf, :password)
        end
end
