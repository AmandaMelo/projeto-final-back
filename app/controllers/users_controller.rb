class UsersController < ApplicationController
  before_action :set_user, only: [:show, :update, :destroy]
  load_and_authorize_resource
  
  # GET /users
  def index
    @users = User.all

    render json: @users
  end

  # GET /users/1
  def show
    render json: @user
  end

  def get_coordD

    render json: @users.coordD

  end

  def get_coordC

    render json: @users.coordC

  end

  # POST /users
  def create
    @user = User.new(user_params)
    
    #senha temporária = 'AAAA-MM-DD'
    @user.password = @user.birthdate.to_s
    @user.password_confirmation = @user.birthdate.to_s

    # definindo a role
    @user.role = 1 if current_user&.coordD?
    @user.role = 0 if current_user&.coordC?


    #salvando
    if @user.save
      # create_role_class(current_user)
      
      render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end


  # PATCH/PUT /users/1
  def update
    unless current_user&.director?
      @user.update(other_params)
      render json: @user
    else
      if @user.update(user_params)
        render json: @user
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
  end

  def create_teacher
    @user = User.new(user_params)
    @user.password = @user.birthdate.to_s
    @user.password_confirmation = @user.birthdate.to_s
    if @user.save
      teacher = Teacher.new(user_id: @user.id, department_id: Department.find_by(user_id: current_user.id).id)
      if teacher.save
        render json: {user: @user, teacher: @user.teacher}
      end
    else
      render json: {user: @user.errors} #,teacher: teacher.errors}
    end
  end

  def create_student
    @user = User.new(user_params)
    @user.password = @user.birthdate.to_s
    @user.password_confirmation = @user.birthdate.to_s
    if @user.save
      student = Student.new(user_id: @user.id, course_id: Course.find_by(user_id: current_user.id).id)
      if student.save
        render json: {user: @user, student: @user.student}
      end
    else
      render json: {user: @user.errors} #,teacher: teacher.errors}
    end
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:name, :email, :cpf, :rg, :role, :birthdate, :state, :nationality, :street, :number, :neighborhood, :complement, :cep, :telephone, :cellphone, :password, :password_confirmation)
    end

    def other_params
      params.require(:user).permit(:name, :street, :number, :neighborhood, :complement, :cep, :telephone, :cellphone, :password, :password_confirmation)
    end  
end
