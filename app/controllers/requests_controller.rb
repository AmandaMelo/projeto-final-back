class RequestsController < ApplicationController
  before_action :set_request, only: [:show, :update, :destroy]
  # load_and_authorize_resource
  # GET /requests
  def index
    @requests = Request.all

    render json: @requests
  end

  # GET /requests/1
  def show
    render json: @request
  end

  # POST /requests
  def create
    @request = Request.new(request_params)
    @request.student_id = Student.find_by(user_id: current_user.id).id
    period_status = Period.find(CollegeClass.find(@request.college_class_id).period_id)
    if period_status.registrations?
      if @request.save
        render json: @request, status: :created, location: @request
      else
        render json: @request.errors, status: :unprocessable_entity
      end
    else
      render json: @request.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /requests/1
  def update
    period_status = Period.find(CollegeClass.find(@request.college_class_id).period_id)
    if period_status.registration?
      if @request.update(request_params)
        render json: @request
      else
        render json: @request.errors, status: :unprocessable_entity
      end
    else
      render json: @request.errors, status: :unprocessable_entity
    end
  end

  # DELETE /requests/1
  def destroy
    @request.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_request
      @request = Request.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def request_params
      params.require(:request).permit(:college_class_id)
    end
end
