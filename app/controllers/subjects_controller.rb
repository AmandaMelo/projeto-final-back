class SubjectsController < ApplicationController
  before_action :set_subject, only: [:show, :update, :destroy]
  # load_and_authorize_resource
  # GET /subjects
  def index
    @subjects = Subject.all

    render json: @subjects
  end

  # GET /subjects/1
  def show
    render json: @subject
  end

  # POST /subjects
  def create
    @subject = Subject.new(subject_params)
    period_status = Period.find(@subject.period_id)
    if period_status.planning?
      @subject.department_id = Department.find_by(user_id: current_user.id).id
      if @subject.save
        render json: @subject, status: :created, location: @subject
      else
        render json: @subject.errors, status: :unprocessable_entity
      end
    else
      render json: @subject.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /subjects/1
  def update
    period_status = Period.find(@subject.period_id)
    if period_status.planning?
      if @subject.update(subject_params)
        render json: @subject
      else
        render json: @subject.errors, status: :unprocessable_entity
      end
    else
      render json: @subject.errors, status: :unprocessable_entity
    end
  end

  # DELETE /subjects/1
  def destroy
    period_status = Period.find(@subject.period_id)
    if period_status.planning?
      @subject.destroy
    end
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subject
      @subject = Subject.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def subject_params
      params.require(:subject).permit(:knowledge_area, :name, :workload, :period_id)
    end
end
