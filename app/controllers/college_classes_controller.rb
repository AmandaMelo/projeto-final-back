class CollegeClassesController < ApplicationController
  before_action :set_college_class, only: [:show, :update, :destroy]
  # load_and_authorize_resource
  # GET /college_classes
  def index
    @college_classes = CollegeClass.all

    render json: @college_classes
  end

  # GET /college_classes/1
  def show
    render json: @college_class
  end

  # POST /college_classes
  def create

    @college_class = CollegeClass.new(college_class_params)
    @college_class.period_id = Subject.find(@college_class.subject_id).period_id

    period_status = Period.find(@college_class.period_id)
    if period_status.planning?
      if @college_class.save
        render json: @college_class, status: :created, location: @college_class
      else
        render json: @college_class.errors, status: :unprocessable_entity
      end
    else
      render json: @college_class.errors, status: :unprocessable_entity
    end
  end
  # PATCH/PUT /college_classes/1
  def update
    period_status = Period.find(@college_class.period_id)
    if period_status.planning?
      if @college_class.update(college_class_params)
        render json: @college_class
      else
        render json: @college_class.errors, status: :unprocessable_entity
      end
    end
  end
  # DELETE /college_classes/1
  def destroy
    period_status = Period.find(@college_class.period_id)
    if period_status.planning?
      @college_class.destroy
    end
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_college_class
      @college_class = CollegeClass.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def college_class_params
      params.require(:college_class).permit(:subject_id, :name, :code, :calendar, :classroom, :teacher_id)
    end
end
