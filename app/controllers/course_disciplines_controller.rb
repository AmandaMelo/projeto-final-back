class CourseDisciplinesController < ApplicationController
  before_action :set_course_discipline, only: [:show, :update, :destroy]
  # load_and_authorize_resource
  # GET /course_disciplines
  def index
    @course_disciplines = CourseDiscipline.all

    render json: @course_disciplines
  end

  # GET /course_disciplines/1
  def show
    render json: @course_discipline
  end

  # POST /course_disciplines
  def create
    @course_discipline = CourseDiscipline.new(course_discipline_params)

    if @course_discipline.save
      render json: @course_discipline, status: :created, location: @course_discipline
    else
      render json: @course_discipline.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /course_disciplines/1
  def update
    if @course_discipline.update(course_discipline_params)
      render json: @course_discipline
    else
      render json: @course_discipline.errors, status: :unprocessable_entity
    end
  end

  # DELETE /course_disciplines/1
  def destroy
    @course_discipline.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_course_discipline
      @course_discipline = CourseDiscipline.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def course_discipline_params
      params.require(:course_discipline).permit(:order, :subject_id, :course_id)
    end
end
