class Vacancy < ApplicationRecord

  #relações
  belongs_to :college_class
  belongs_to :course
  has_many :subscription

  #validações
  validates :college_class_id, :number, :course_id, presence: true
end
