class Department < ApplicationRecord

    #relações
    has_many :subjects
    belongs_to :user  

    #validação
    validates :code, :campus, :knowledge_area, :name, presence: true
    validates :user_id, presence: true, uniqueness: true

end
