class CompletedSubject < ApplicationRecord

  # relações
  belongs_to :student
  belongs_to :subject

  #validação
  validates :student_id, :subject_id, presence: true

end
