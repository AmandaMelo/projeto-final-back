class Prerequisite < ApplicationRecord

  #relações
  belongs_to :subject

  #validação
  validates :subject_id, :requisite, presence: true
end
