class Subscription < ApplicationRecord

  #relações
  belongs_to :student
  belongs_to :vacancy
  belongs_to :period

  #validações
  validates :student_id, :vacancy_id, :teacher_id, presence: true
  after_save :less_vacancies
  after_update :subscription_finish

  #funções
  enum status: {progress: 0, failed: 1, approved: 2}

  def subscription_finish
    if grade_one != nil && grade_two != nil
      self.final_score = (grade_one + grade_two)/2
      if self.final_score >= 6
        self.status = 2
        CompletedSubject.create(student_id: student_id, subject_id: CollegeClass.find(Vacancy.find(vacancy_id).college_class_id).subject_id)
      else
        self.status = 1
      end
      self.save
    else
      errors.add(:empity_grades, "As notas devem ser preenchidas.")
    end
  end

  def less_vacancies
    vacancy = Vacancy.find(vacancy_id)
    vacancy_number = vacancy.number - 1
    vacancy.update_attribute(:number, vancancy_number)
  end
end