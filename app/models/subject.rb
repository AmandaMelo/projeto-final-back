class Subject < ApplicationRecord

  #relações
  belongs_to :department
  belongs_to :period
  has_many :prerequisites
  has_many :college_classes
  
  #validações
  validates :workload, :knowledge_area, :department_id, :period_id, presence: true
  validates :name, presence: true, uniqueness: true

end
