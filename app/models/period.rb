class Period < ApplicationRecord

    #relações
    has_many :college_classes
    has_many :subjects
    has_many :subscriptions

    #validações
    validates :status,:year ,:semester, presence: true
    validate :onlyperiod

    #funções
    enum status: {planning: 0, registrations: 1, progress: 2, concluded: 3}

    def onlyperiod
        if Period.find_by(year: year, semester: semester).present?
          errors.add :double, "Esse período já existe."
        end
    end

end
