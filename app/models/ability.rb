# frozen_string_literal: true

class Ability
  include CanCan::Ability
  def initialize(user)

    user ||= User.new
    
    can [:read], Course
    can [:read], Period
    can [:update, :read, :get_coordD, :get_coordC], user

    if user.director?
      can :manage, User, role: 3
      can :manage, User, role: 2
      can :manage, Period
      can :manage, Course
      can :manage, Department
    elsif user.coordD?
      can [:read, :update], Department, user_id: user.id
      can [:create, :read, :create_teacher], User, role: 1
      can :manage, Teacher
      can [:create, :read], License
      can :manage, Subject
      can :manage, CollegeClass
      can :manage, Vacancy
      can :manage, Subscription
      
    
    elsif user.coordC?
      course_id = Course.find_by(user_id: user.id).id
      can [:read, :update], Course, id: course_id
      can [:create, :update, :read, :create_student], User, role: 0
      can :manage, Subscription

    elsif user.teacher?
      teacher = Teacher.find_by(user_id: user.id).id
      can [:update, :read], Subscription, teacher_id: teacher

    elsif user.student?
      student = Student.find_by(user_id: user.id).id
      can [:read], Subscription, student_id: student
      can :manage, Request, student
    end
  end
end
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities

