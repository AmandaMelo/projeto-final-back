class User < ApplicationRecord
    
    #relações
    has_secure_password
    has_one :teacher
    has_one :student
    has_many :subscriptions
    has_one :department
    
    #validações
    validates :name, :state, :nationality, :birthdate, :role, presence: true
    validates :email, :cpf, :rg, presence: true, uniqueness: true
    validates :email, format: {with: /\b[A-Z0-9._%a-z\-]+@id\.uff\.br\z/, message: "O email deve ser do domínio idUFF"}
    validates :cpf, format: {with: /\b([0-9]{3}+.){2}+[0-9]{3}+-+[0-9]{2}\z/, message: "O CPF deve estar no formato 999.999.999-99"}
    validates :password, length: {minimum:6}, on: :update
   
    before_save {self.email = email.downcase}
    
    #funções
    enum role: {student: 0, teacher: 1, coordD: 2, coordC: 3, director: 4}

end
