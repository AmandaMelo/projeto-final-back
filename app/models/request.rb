class Request < ApplicationRecord

  #relações
  belongs_to :student
  belongs_to :college_class

  #validações
  validates :student_id, :college_class_id, presence: true
  validate :onlyrequest
  before_save :belongs_to_course
  before_save :verify_prerequisites
  after_validation :has_vacancy
  

  #funções
  def verify_prerequisites
    subject_id = CollegeClass.find(college_class_id).subject_id
    prerequisites_subject = Prerequisite.where(subject_id: subject_id).select("requisite")
    prerequisites = []
    prerequisites_subject.each do |r| 
      prerequisites.append(r.requisite) 
    end

    completed_subjects = CompletedSubject.where(student_id: student_id).select("subject_id")
    student_subjects = []
    completed_subjects.each do |s| 
      student_subjects.append(s.subject_id) 
    end

    allowed = true
    prerequisites.each do |r| 
      unless student_subjects.include? r
        allowed = false 
      end
    end

    unless allowed
      errors.add(:request_not_allowed, "Inscrição não permitida, aluno não possui os pré-requisitos necessários.")
    end
  end

  def belongs_to_course
    all_vacancies = Vacancy.where(college_class_id: college_class_id).select("course_id")
    available_courses = []
    all_vacancies.each do |v| 
      available_courses.append(v.course_id)
    end

    student_course = Student.find(student_id).course_id

    unless available_courses.include? student_course
      errors.add(:no_available, "Inscrição não permitida, essa turma não possui vagas para o seu curso.")
    end
  end
  
  def has_vacancy
    course_id = Student.find(student_id).course_id
    vacancy = Vacancy.find_by(college_class_id: college_class_id, course_id: course_id)

    if vacancy.number == 0
      errors.add(:no_vacancy, "Não há mais vagas disponíveis nessa turma.")
    end
  end

  def onlyrequest
    if Request.find_by(student_id: student_id, college_class_id: college_class_id ).present?
      errors.add :double, "Esse pedido de inscrição já existe"
    end
  end

end
