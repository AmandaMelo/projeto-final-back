class CollegeClass < ApplicationRecord

  #relações
  belongs_to :teacher
  belongs_to :subject
  belongs_to :period
  before_save :see_license

  #validação
  validates :subject_id, :name, :code, :calendar, :classroom, :teacher_id, presence: true
  validate :onlycollege_class

  #funções
  def see_license
    unless License.find_by(teacher_id: teacher_id, subject_id: subject_id)&.present?
      errors.add(:cant_use_this_teacher, "O professor não tem licensa para dar aula nessa turma")
    end
  end

  def onlycollege_class
    if CollegeClass.find_by(subject_id: subject_id, teacher_id: teacher_id,code: code).present?
      errors.add :double, "Essa turma já existe já existe"
    end
  end
end
