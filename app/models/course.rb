class Course < ApplicationRecord

    #relações
    belongs_to :user
    has_many :students
    has_many :course_disciplines
    has_many :vacancies

    #validação
    validates :code, :campus, :knowledge_area, :name, presence: true
    validates :user_id, presence: true, uniqueness: true

end
