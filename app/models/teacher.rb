class Teacher < ApplicationRecord

  #relações
  belongs_to :user
  belongs_to :department
  has_many :licenses

  #validações
  validates :user_id, :department_id, presence: true
  
end
