class Student < ApplicationRecord

  #relações
  belongs_to :user
  belongs_to :course
  has_many :subscriptions
  has_many :requests
  has_many :completed_subjects
  
  #validações
  after_create :counter_students
  validates :user_id, :course_id, presence: true

  #funções
  def counter_students
    course = Course.find(self.course_id)
    number = course.number_students + 1
    course.update_attribute(:number_students, number)
  end
end
