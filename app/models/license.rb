class License < ApplicationRecord

  #relações
  belongs_to :subject
  belongs_to :teacher

  #validações
  validate :onlylicense
  validates :subject_id, :teacher_id, presence: true

  #funções
  def onlylicense
    if License.find_by(subject_id: subject_id).present?
      errors.add :double, "Licensa já existe"
    end
  end
end
