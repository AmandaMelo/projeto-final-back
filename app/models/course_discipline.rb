class CourseDiscipline < ApplicationRecord

  # relações
  belongs_to :subject
  belongs_to :course

  #validação
  validates :order, :subject_id, :course_id, presence: true

end
