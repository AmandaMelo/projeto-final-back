# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_01_29_151918) do

  create_table "college_classes", force: :cascade do |t|
    t.integer "subject_id"
    t.string "name"
    t.string "code"
    t.string "calendar"
    t.string "classroom"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "teacher_id"
    t.integer "period_id"
    t.index ["period_id"], name: "index_college_classes_on_period_id"
    t.index ["subject_id"], name: "index_college_classes_on_subject_id"
    t.index ["teacher_id"], name: "index_college_classes_on_teacher_id"
  end

  create_table "completed_subjects", force: :cascade do |t|
    t.integer "student_id"
    t.integer "subject_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["student_id"], name: "index_completed_subjects_on_student_id"
    t.index ["subject_id"], name: "index_completed_subjects_on_subject_id"
  end

  create_table "course_disciplines", force: :cascade do |t|
    t.integer "order"
    t.integer "subject_id"
    t.integer "course_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_course_disciplines_on_course_id"
    t.index ["subject_id"], name: "index_course_disciplines_on_subject_id"
  end

  create_table "courses", force: :cascade do |t|
    t.string "code"
    t.string "campus"
    t.string "knowledge_area"
    t.string "name"
    t.integer "number_students", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "period_id"
    t.integer "user_id"
    t.index ["period_id"], name: "index_courses_on_period_id"
    t.index ["user_id"], name: "index_courses_on_user_id"
  end

  create_table "departments", force: :cascade do |t|
    t.string "code"
    t.string "campus"
    t.string "knowledge_area"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.integer "period_id"
    t.index ["period_id"], name: "index_departments_on_period_id"
    t.index ["user_id"], name: "index_departments_on_user_id"
  end

  create_table "licenses", force: :cascade do |t|
    t.integer "subject_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "teacher_id"
    t.index ["subject_id"], name: "index_licenses_on_subject_id"
    t.index ["teacher_id"], name: "index_licenses_on_teacher_id"
  end

  create_table "periods", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "year", default: "2021"
    t.string "semester", default: ".1"
    t.integer "status", default: 0
  end

  create_table "prerequisites", force: :cascade do |t|
    t.integer "subject_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "requisite"
    t.index ["subject_id"], name: "index_prerequisites_on_subject_id"
  end

  create_table "requests", force: :cascade do |t|
    t.integer "student_id"
    t.integer "college_class_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["college_class_id"], name: "index_requests_on_college_class_id"
    t.index ["student_id"], name: "index_requests_on_student_id"
  end

  create_table "students", force: :cascade do |t|
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "course_id"
    t.index ["course_id"], name: "index_students_on_course_id"
    t.index ["user_id"], name: "index_students_on_user_id"
  end

  create_table "subjects", force: :cascade do |t|
    t.string "knowledge_area"
    t.string "name"
    t.string "workload"
    t.integer "period_id"
    t.integer "department_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["department_id"], name: "index_subjects_on_department_id"
    t.index ["period_id"], name: "index_subjects_on_period_id"
  end

  create_table "subscriptions", force: :cascade do |t|
    t.float "final_score"
    t.float "grade_one"
    t.float "grade_two"
    t.integer "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "period_id"
    t.integer "student_id"
    t.integer "vacancy_id"
    t.integer "teacher_id"
    t.index ["period_id"], name: "index_subscriptions_on_period_id"
    t.index ["student_id"], name: "index_subscriptions_on_student_id"
    t.index ["teacher_id"], name: "index_subscriptions_on_teacher_id"
    t.index ["vacancy_id"], name: "index_subscriptions_on_vacancy_id"
  end

  create_table "teachers", force: :cascade do |t|
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "department_id"
    t.index ["department_id"], name: "index_teachers_on_department_id"
    t.index ["user_id"], name: "index_teachers_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "cpf"
    t.string "rg"
    t.integer "role"
    t.date "birthdate"
    t.string "state"
    t.string "nationality"
    t.string "street"
    t.integer "number"
    t.string "neighborhood"
    t.string "complement"
    t.string "cep"
    t.string "telephone"
    t.string "cellphone"
    t.string "password_digest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "vacancies", force: :cascade do |t|
    t.integer "college_class_id"
    t.integer "number"
    t.integer "course_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["college_class_id"], name: "index_vacancies_on_college_class_id"
    t.index ["course_id"], name: "index_vacancies_on_course_id"
  end

end
