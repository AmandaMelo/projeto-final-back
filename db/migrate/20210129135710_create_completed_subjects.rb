class CreateCompletedSubjects < ActiveRecord::Migration[5.2]
  def change
    create_table :completed_subjects do |t|
      t.references :student, foreign_key: true
      t.references :subject, foreign_key: true

      t.timestamps
    end
  end
end
