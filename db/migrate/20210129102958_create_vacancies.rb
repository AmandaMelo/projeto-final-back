class CreateVacancies < ActiveRecord::Migration[5.2]
  def change
    create_table :vacancies do |t|
      t.references :college_class, foreign_key: true
      t.integer :number
      t.references :course, foreign_key: true

      t.timestamps
    end
  end
end
