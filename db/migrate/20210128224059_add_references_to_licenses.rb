class AddReferencesToLicenses < ActiveRecord::Migration[5.2]
  def change
    add_reference :licenses, :teacher, foreign_key: true
  end
end
