class ChangeTypeColumnsToPeriods < ActiveRecord::Migration[5.2]
  def change
    remove_column :periods, :year
    add_column :periods, :year, :string, default: Date.today.to_s.split('-')[0]
    
    remove_column :periods, :semester
    add_column :periods, :semester, :string, default: '.1'

    remove_column :periods, :status
    add_column :periods, :status, :integer, default: 0
    #Ex:- add_column("admin_users", "username", :string, :limit =>25, :after => "email")
  end
end
