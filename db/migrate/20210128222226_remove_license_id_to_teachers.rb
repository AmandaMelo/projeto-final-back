class RemoveLicenseIdToTeachers < ActiveRecord::Migration[5.2]
  def change
    remove_reference :teachers, :license, foreign_key: true
  end
end
