class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :cpf
      t.string :rg
      t.integer :role
      t.date :birthdate
      t.string :state
      t.string :nationality
      t.string :street
      t.integer :number
      t.string :neighborhood
      t.string :complement
      t.string :cep
      t.string :telephone
      t.string :cellphone
      t.string :password_digest

      t.timestamps
    end
  end
end
