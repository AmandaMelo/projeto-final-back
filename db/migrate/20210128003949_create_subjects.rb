class CreateSubjects < ActiveRecord::Migration[5.2]
  def change
    create_table :subjects do |t|
      t.string :knowledge_area
      t.string :name
      t.string :workload
      t.references :period, foreign_key: true
      t.references :department, foreign_key: true

      t.timestamps
    end
  end
end
