class CreateCourses < ActiveRecord::Migration[5.2]
  def change
    create_table :courses do |t|
      t.string :code
      t.string :campus
      t.string :knowledge_area
      t.string :name
      t.integer :number_students

      t.timestamps
    end
  end
end
