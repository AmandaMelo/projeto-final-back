class CreateSubscriptions < ActiveRecord::Migration[5.2]
  def change
    create_table :subscriptions do |t|
      t.float :final_score
      t.float :grade_one
      t.float :grade_two
      t.integer :status
      t.references :college_class, foreign_key: true
      t.references :students, foreign_key: true

      t.timestamps
    end
  end
end
