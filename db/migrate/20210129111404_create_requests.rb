class CreateRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :requests do |t|
      t.references :student, foreign_key: true
      t.references :college_class, foreign_key: true

      t.timestamps
    end
  end
end
