class RenameReferencesToSubscriptions < ActiveRecord::Migration[5.2]
  def change
    remove_reference :subscriptions, :students, foreign_key: true, index: true
    add_reference :subscriptions, :student, foreign_key: true, index: true
  end
end
