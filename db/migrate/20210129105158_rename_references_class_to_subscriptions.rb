class RenameReferencesClassToSubscriptions < ActiveRecord::Migration[5.2]
  def change
    remove_reference :subscriptions, :college_class, foreign_key: true, index: true
    add_reference :subscriptions, :vacancy, foreign_key: true, index: true
  end
end
