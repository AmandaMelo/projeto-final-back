class AddReferencesToDepartment < ActiveRecord::Migration[5.2]
  def change
    add_reference :departments, :user, foreign_key: true
    add_reference :departments, :period, foreign_key: true
  end
end
