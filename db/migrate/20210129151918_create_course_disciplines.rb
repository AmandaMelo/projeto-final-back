class CreateCourseDisciplines < ActiveRecord::Migration[5.2]
  def change
    create_table :course_disciplines do |t|
      t.integer :order
      t.references :subject, foreign_key: true
      t.references :course, foreign_key: true

      t.timestamps
    end
  end
end
