class AddReferencesToSubscriptions < ActiveRecord::Migration[5.2]
  def change
    add_reference :subscriptions, :period, foreign_key: true
  end
end
