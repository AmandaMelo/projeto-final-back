class CreateCollegeClasses < ActiveRecord::Migration[5.2]
  def change
    create_table :college_classes do |t|
      t.references :subject, foreign_key: true
      t.string :name
      t.string :code
      t.string :calendar
      t.string :classroom

      t.timestamps
    end
  end
end
