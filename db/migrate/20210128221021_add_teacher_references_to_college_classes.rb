class AddTeacherReferencesToCollegeClasses < ActiveRecord::Migration[5.2]
  def change
    add_reference :college_classes, :teacher, foreign_key: true
  end
end
