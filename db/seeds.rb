# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create(name: 'Antonio Claudio Lucas da Nóbrega', email: 'reitor@id.uff.br', cpf: '111.222.333-44', rg: '11.222.333-4', birthdate: '16/04/1965', state: 'Rio de Janeiro', nationality: 'Brasileiro', role: 4, password: '123456', password_confirmation: '123456')
# User.create(name: 'Amandinha', email: 'ammelo@id.uff.br', cpf: '111.222.333-45', rg: '11.222.333-5', birthdate: '06/02/2002', state: 'Rio de Janeiro', nationality: 'Brasileira', role: 3, password: '123456', password_confirmation: '123456')
# User.create(name: 'Leozinho Amor da Minha Vida', email: 'leoamorzinho@id.uff.br', cpf: '111.222.333-46', rg: '11.222.333-6', birthdate: '22/11/2000', state: 'Rio de Janeiro', nationality: 'Brasileiro', role: 0, password: '123456', password_confirmation: '123456')
# User.create(name: 'Matheus agressivo', email: 'math@id.uff.br', cpf: '111.222.333-47', rg: '11.222.333-7', birthdate: '21/04/2001', state: 'Rio de Janeiro', nationality: 'Brasileiro', role: 2, password: '123456', password_confirmation: '123456')
# User.create(name: 'Luizinho', email: 'luiz@id.uff.br', cpf: '111.222.333-48', rg: '11.222.333-8', birthdate: '21/04/2000', state: 'Rio de Janeiro', nationality: 'Brasileiro', role: 1, password: '123456', password_confirmation: '123456')
# Period.create(year: '2020', semester: '.1', status: 0)
# Course.create(code: 'TCC00031', campus: 'Praia Vermelha', knowledge_area: "sla", name: 'Computação', number_students: 0, period_id: 1, user_id: 2)
# Student.create(user_id: 3, course_id: 1)
# Department.create(code: 'tco002', campus: 'gragoata', knowledge_area: 'não tem', name:'departamento da burrice', user_id: 4, period_id: 1)
# Subject.create(knowledge_area: 'n sei', name: 'calculo', workload: 'aaa', period_id: 1, department_id: 1)
# CollegeClass.create(subject_id: 1, name: 'A1', code: 'tcc01', calendar: 'seg,terça',classroom:'IC-304')
# License.create(subject_id: 1)
# Prerequisite.create(subject_id: 1)
# Vacancy.create(college_class_id: 1, number: 30, course_id: 1)
# Subscription.create(final_score: nil, grade_one: nil, grade_two: nil, status: 0, vacancy_id: 1, student_id: 1, teacher_id: 1)
# Teacher.create(user_id: 5)