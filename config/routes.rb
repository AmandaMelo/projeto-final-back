Rails.application.routes.draw do
  resources :course_disciplines
  resources :completed_subjects
  resources :requests
  resources :vacancies
  post '/login', to: 'authentication#login'
  resources :subscriptions
  resources :college_classes
  resources :teachers
  resources :licenses
  resources :prerequisites
  resources :subjects
  resources :departments
  resources :courses
  resources :periods
  resources :students
  resources :users
  post 'create_teacher', to: 'users#create_teacher'
  post 'create_student', to: 'users#create_student'
  get 'get_coordD', to: 'users#get_coordD'
  get 'get_coordC', to: 'users#get_coordC'
  get 'show_course_coordC', to: 'courses#show_course_coordC'
  get 'show_department_coordD', to: 'departments#show_department_coordD'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
