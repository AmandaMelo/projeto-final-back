require 'test_helper'

class CollegeClassesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @college_class = college_classes(:one)
  end

  test "should get index" do
    get college_classes_url, as: :json
    assert_response :success
  end

  test "should create college_class" do
    assert_difference('CollegeClass.count') do
      post college_classes_url, params: { college_class: { calendar: @college_class.calendar, classroom: @college_class.classroom, code: @college_class.code, name: @college_class.name, subject_id: @college_class.subject_id } }, as: :json
    end

    assert_response 201
  end

  test "should show college_class" do
    get college_class_url(@college_class), as: :json
    assert_response :success
  end

  test "should update college_class" do
    patch college_class_url(@college_class), params: { college_class: { calendar: @college_class.calendar, classroom: @college_class.classroom, code: @college_class.code, name: @college_class.name, subject_id: @college_class.subject_id } }, as: :json
    assert_response 200
  end

  test "should destroy college_class" do
    assert_difference('CollegeClass.count', -1) do
      delete college_class_url(@college_class), as: :json
    end

    assert_response 204
  end
end
