require 'test_helper'

class CourseDisciplinesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @course_discipline = course_disciplines(:one)
  end

  test "should get index" do
    get course_disciplines_url, as: :json
    assert_response :success
  end

  test "should create course_discipline" do
    assert_difference('CourseDiscipline.count') do
      post course_disciplines_url, params: { course_discipline: { course_id: @course_discipline.course_id, order: @course_discipline.order, subject_id: @course_discipline.subject_id } }, as: :json
    end

    assert_response 201
  end

  test "should show course_discipline" do
    get course_discipline_url(@course_discipline), as: :json
    assert_response :success
  end

  test "should update course_discipline" do
    patch course_discipline_url(@course_discipline), params: { course_discipline: { course_id: @course_discipline.course_id, order: @course_discipline.order, subject_id: @course_discipline.subject_id } }, as: :json
    assert_response 200
  end

  test "should destroy course_discipline" do
    assert_difference('CourseDiscipline.count', -1) do
      delete course_discipline_url(@course_discipline), as: :json
    end

    assert_response 204
  end
end
