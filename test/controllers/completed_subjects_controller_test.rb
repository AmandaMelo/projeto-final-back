require 'test_helper'

class CompletedSubjectsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @completed_subject = completed_subjects(:one)
  end

  test "should get index" do
    get completed_subjects_url, as: :json
    assert_response :success
  end

  test "should create completed_subject" do
    assert_difference('CompletedSubject.count') do
      post completed_subjects_url, params: { completed_subject: { student_id: @completed_subject.student_id, subject_id: @completed_subject.subject_id } }, as: :json
    end

    assert_response 201
  end

  test "should show completed_subject" do
    get completed_subject_url(@completed_subject), as: :json
    assert_response :success
  end

  test "should update completed_subject" do
    patch completed_subject_url(@completed_subject), params: { completed_subject: { student_id: @completed_subject.student_id, subject_id: @completed_subject.subject_id } }, as: :json
    assert_response 200
  end

  test "should destroy completed_subject" do
    assert_difference('CompletedSubject.count', -1) do
      delete completed_subject_url(@completed_subject), as: :json
    end

    assert_response 204
  end
end
